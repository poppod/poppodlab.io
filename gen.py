# coding: utf-8
import glob
import xml.etree.ElementTree as ET
from jinja2 import Environment, FileSystemLoader

pages = []
for name in glob.glob('xml/*.xml'):
    
    root = ET.parse(name).getroot()

    channel = root.find('channel')

    page = {}
    page['title'] = channel.find('title').text
    page['url'] = page['title'].replace(' ','_')+'.html'
    page['description'] = channel.find('description').text

    page['podlist'] = []
    for item in channel.findall('item'): 
        item = {'title': item.find('title').text,
                'url': item.find('link').text,
                }
        page['podlist'].append(item)
    pages.append(page)


menu = [{'url':page['url'], 'name': page['title']} for page in pages]

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)
template = env.get_template('page.jinja')
for page in pages:
    output = template.render(page=page, menu=menu)
    with open('public/'+page['url'],'w') as f:
        f.write(output)
